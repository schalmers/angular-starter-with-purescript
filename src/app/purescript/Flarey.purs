module Flarey
  ( combineTextEle
  ) where

import Prelude (Unit, (<>), pure)

import Control.Monad.Eff (Eff)

import Signal.Channel (CHANNEL)
import DOM (DOM)

import Flare (UI, string_, runFlareWith)

type FlareEff eff =
  Eff ( dom :: DOM
      , channel :: CHANNEL
      | eff
      )

-- This is the lie
type InputFn = forall eff. String -> Eff eff Unit

combineTextUI :: forall e. UI e String
combineTextUI = string_ "" <> pure " " <> string_ ""

combineTextEle :: forall eff. String -> InputFn -> FlareEff eff Unit
combineTextEle elemId fn = runFlareWith elemId fn combineTextUI
