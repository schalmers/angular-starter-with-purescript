module Basic (mangler) where

import Prelude

mangler :: String -> String
mangler s = s <> s
